class Shader {
public:
    int type;
    Shader(int type);
    ~Shader();
};

template<typename ...Shader>
class ShaderProgram {
public:
    ShaderProgram(Shader&& ...shaders);
    ~ShaderProgram();
};

template<typename ...Shader>
ShaderProgram<Shader...> make_shader_program(const Shader&& ...shaders);