#include "./Shader.hpp"
#include <utility>
#include <iostream>

Shader::Shader(int type) : type{type} {
    std::cout << "Creating Shader " << type << std::endl;
}

Shader::~Shader() {
    std::cout << "Destroying Shader " << type << std::endl;
}

template<typename ...Shader>
ShaderProgram<Shader...>::ShaderProgram(Shader&& ...shaders) {
    std::puts(__FUNCSIG__);
    std::puts("Shader Program Construction");
}

template<typename ...Shader>
ShaderProgram<Shader...>::~ShaderProgram() {
    std::puts("Shader Program Destruction");
}

template<typename ...Args>
ShaderProgram<Args...> make_shader_program(Args&& ...shaders) {
    return ShaderProgram<Args...>(
        (
            [shaders]{
                static_assert(std::is_same_v<Shader, std::decay_t<Args>>,
                    "Argument is not a Shader");
            }(),
            std::forward<Args>(shaders)
        )...
    );
}

int main() {
    Shader test{1};
    auto shader = make_shader_program(test, Shader{2});
    return 0;
}